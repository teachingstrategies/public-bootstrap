#!/bin/bash

yum -y remove "kernel-*"
rpm -e "kernel-ml-$(uname -r)"

if ! grep -q 'GRUB_DEFAULT=0' /etc/default/grub; then
    sed -i 's/GRUB_DEFAULT=./GRUB_DEFAULT=0/' /etc/default/grub;
fi

pushd /tmp &> /dev/null
    rpm_file="kernel-lt-5.4.242-1.el7.elrepo.x86_64.rpm"
    curl "https://bitbucket.org/teachingstrategies/public-bootstrap/raw/master/rpms/$rpm_file" --output "$rpm_file" --silent
    rpm -U --force "$rpm_file"
    rm -f "$rpm_file"
popd &> /dev/null
grub2-mkconfig -o /boot/grub2/grub.cfg
shutdown -r now
