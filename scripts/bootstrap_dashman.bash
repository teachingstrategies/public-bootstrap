#!/bin/bash

# This script will initiate or refresh a masterless Salt minion on:
## CentOS 7.x x86_64
## Ubuntu 14.x x86_64 LTS

# set -x

BOOTSTRAP_DIR='/opt/ts-bootstrap'
GIT_CONF_DIR="${BOOTSTRAP_DIR}/git"
GIT_RSAKEY_FILE="${GIT_CONF_DIR}/git.pem"
GIT_SSHCONFIG_FILE="${GIT_CONF_DIR}/git_ssh"
GIT_LOCAL_DIR="${GIT_CONF_DIR}/ts-bootstrap"
HOST_INFO="${BOOTSTRAP_DIR}/id"

mkdir -p $GIT_CONF_DIR
chmod 700 $BOOTSTRAP_DIR

# Determine the Linux distribution
DISTRO=""
[ -f /etc/redhat-release ] && DISTRO="CentOS"
[ -f /etc/debian_version ] && DISTRO="Ubuntu"

if [ -z "$DISTRO" ]; then
    echo "Unable to determine Linux distribution -- exiting." 
    exit 1
fi

[ -n "${HOST}" ] && echo $HOST > ${HOST_INFO}

# Make sure Git/salt-minion packages are installed and latest security updates are applied
if [ "$DISTRO" = "CentOS" ]; then
    easy_install pip
    pip install awscli
    yum install -y git

    rpm -qa | grep -qi epel
    if [ $? != 0 ]; then
            file_name="epel-release-7-5.noarch.rpm"
            curl http://dl.fedoraproject.org/pub/epel/7/x86_64/e/$file_name > /tmp/$file_name
            rpm -Uvh /tmp/$file_name
            rm -f /tmp/$file_name
    fi
    rpm -qa | grep -qi salt-minion
    if [ $? != 0 ]; then
            yum -y install salt-minion
    fi
    yum -y update
fi
if [ "$DISTRO" = "Ubuntu" ]; then
    apt-get update
    apt-get -y install python-pip git
    pip install awscli

    ls /etc/apt/sources.list.d/ | grep -qi saltstack
    if [ $? != 0 ]; then
            apt-add-repository -y ppa:saltstack/salt
            apt-get update
    fi
    dpkg --list | grep -qi salt-minion
    if [ $? != 0 ]; then
            apt-get install -y salt-minion
    fi
    service salt-minion stop
    echo manual > /etc/init/salt-minion.override
    apt-get -y upgrade
fi

GIT_SSHCONFIG="ssh -i $GIT_RSAKEY_FILE -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \$*"
if [ ! -d "$GIT_CONF_DIR" ]; then
    mkdir -p $GIT_CONF_DIR
fi
if [ ! -s "$GIT_RSAKEY_FILE" ]; then
    aws s3 cp s3://ts-common-bootstrap/git.pem $GIT_RSAKEY_FILE
    chmod 0400 $GIT_RSAKEY_FILE
fi
if [ ! -s "$GIT_SSHCONFIG_FILE" ]; then
    echo "$GIT_SSHCONFIG" > $GIT_SSHCONFIG_FILE
    chmod 0700 $GIT_SSHCONFIG_FILE
fi

export GIT_SSH=$GIT_SSHCONFIG_FILE
if [ ! -d "${GIT_LOCAL_DIR}/.git" ]; then
    git clone git@bitbucket.org:teachingstrategies/ts-bootstrap.git $GIT_LOCAL_DIR
else
    cd $GIT_LOCAL_DIR
    git fetch origin
    git reset --hard origin/master
    git clean -d -f
    git checkout master
    git pull
fi

[ ! -d "/srv/salt" ] && ln -s $GIT_LOCAL_DIR/salt /srv/salt
[ ! -d "/srv/pillar" ] && ln -s $GIT_LOCAL_DIR/pillar /srv/pillar

# Configure and run the masterless Salt minion
MINION_ID=''
if [ -s "${HOST_INFO}" ]; then
    MINION_ID=$( cat ${HOST_INFO} )
else
    MINION_ID="UNKNOWN"
fi
echo -e "id: ${MINION_ID}\nfile_client: local\nfile_roots:\n  base:\n    - /srv/salt" > /etc/salt/minion
salt-call state.highstate
