#!/bin/bash

# This script will initiate or refresh a masterless Salt minion on:
## CentOS 7.x x86_64
## Ubuntu 14.x x86_64 LTS
# It should be invoked as:
## curl https://bitbucket.org/teachingstrategies/public-bootstrap/raw/master/scripts/bootstrap.bash | HOST={{HOSTNAME}} bash

# set -x
{ #Ensure the whole script is run

TS_DIR='/opt/ts'
GIT_DIR="${TS_DIR}/git"
GIT_RSAKEY_FILE="${GIT_DIR}/git.pem"
GIT_SSHCONFIG_FILE="${GIT_DIR}/git_ssh"
GIT_REPO_DIR="${GIT_DIR}/ts-bootstrap"
ID_FILE="${TS_DIR}/id"
BOOTSTRAP_BUCKET="ts-common-bootstrap"

# Make sure the environment is prepared
[ -d "$TS_DIR" ] || mkdir -p $TS_DIR
[ -d "$GIT_DIR" ] || mkdir -p $GIT_DIR

# Identify the server - if the HOST variable is empty or unset a default ID of 'UNKNOWN' will be assigned so the server at least gets the base Salt states applied
: ${HOST:=UNKNOWN}
[ -s "$ID_FILE" ] || echo "$HOST" > $ID_FILE
[ "$(cat $ID_FILE)" = "$HOST" ] || { echo "Conflicting HOST parameter between CLI and contents of $ID_FILE - exiting."; exit 1; }

# Determine the Linux distribution
DISTRO=''
[ -f /etc/redhat-release ] && DISTRO="CentOS"
[ -f /etc/debian_version ] && DISTRO="Ubuntu"
[ -z "$DISTRO" ] && { echo "Unable to determine Linux distribution -- exiting."; exit 1; }


# Make sure awscli/Git/salt-minion packages are installed and latest package updates are applied
if [ "$DISTRO" = "CentOS" ]; then
    which pip > /dev/null 2>&1 || { yum -y install python-setuptools python-setuptools-devel; easy_install pip; }
    which aws > /dev/null 2>&1 || pip install awscli
    pip install boto
    rpm -q git > /dev/null 2>&1 || yum -y install git
    rpm -q epel-release > /dev/null 2>&1  || rpm -i https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/e/epel-release-7-11.noarch.rpm
    rpm -q salt-minion > /dev/null 2>&1 || yum -y install salt-minion
    yum -y update
fi
if [ "$DISTRO" = "Ubuntu" ]; then
    dpkg --list | grep -q "^ii  python-pip " || apt-get -y install python-pip
    which aws > /dev/null 2>&1 || pip install awscli
    dpkg --list | grep -q "^ii  git " || apt-get -y install git
    ls /etc/apt/sources.list.d/ | grep -qi saltstack || { apt-add-repository -y ppa:saltstack/salt; apt-get update; }
    dpkg --list | grep -qi salt-minion || { 
        apt-get install -y salt-minion
        service salt-minion stop
        echo manual > /etc/init/salt-minion.override
    }
    apt-get update
    apt-get -y upgrade
fi

# Git configuration & cloning/refreshing
GIT_SSHCONFIG="ssh -i $GIT_RSAKEY_FILE -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \$*"
[ -s "$GIT_RSAKEY_FILE" ] || aws s3 cp s3://${BOOTSTRAP_BUCKET}/git.pem $GIT_RSAKEY_FILE
[ "$(stat -c %a $GIT_RSAKEY_FILE)" = "400" ] || chmod 0400 $GIT_RSAKEY_FILE
[ -s "$GIT_SSHCONFIG_FILE" ] || echo "$GIT_SSHCONFIG" > $GIT_SSHCONFIG_FILE
[ "$(stat -c %a $GIT_SSHCONFIG_FILE)" = "700" ] || chmod 0700 $GIT_SSHCONFIG_FILE
export GIT_SSH=$GIT_SSHCONFIG_FILE
if [ ! -d "${GIT_REPO_DIR}/.git" ]; then
    git clone git@bitbucket.org:teachingstrategies/ts-bootstrap.git $GIT_REPO_DIR
else
    cd $GIT_REPO_DIR
    git fetch origin
    git reset --hard origin/master
    git clean -d -f
    git checkout master
    git pull
fi

# Secure the Teaching Strategies directories and files so they are unreadable by non-root users
find ${TS_DIR} -type f -not -iwholename "$GIT_SSHCONFIG_FILE" -exec chmod 0400 '{}' \;
find ${TS_DIR} -type d -exec chmod 0500 '{}' \;

# Make sure the symlinks from /srv exist and reference the correct target
[ -L "/srv/salt" -a "$(readlink /srv/salt)" = "${GIT_REPO_DIR}/salt" ] || ln -sf ${GIT_REPO_DIR}/salt /srv/salt
[ -L "/srv/pillar" -a "$(readlink /srv/pillar)" = "${GIT_REPO_DIR}/pillar" ] || ln -sf ${GIT_REPO_DIR}/pillar /srv/pillar

# Configure and run the masterless Salt minion
echo -e "id: $(cat $ID_FILE)\nfile_client: local\nfile_roots:\n  base:\n    - /srv/salt" > /etc/salt/minion
salt-call state.highstate

}
