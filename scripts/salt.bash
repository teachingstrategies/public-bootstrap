DIR='/opt/ts-bootstrap'

mkdir -p $DIR

[ -f /etc/redhat-release ] && { easy_install pip; pip install awscli; }
[ -f /etc/debian_version ] && { apt-get update; apt-get -y install python-pip git; }

aws s3 cp s3://ts-common-bootstrap/run_salt.bash ${DIR}/run_salt.bash
bash ${DIR}/run_salt.bash