#!/bin/bash

{ #Ensure the whole script is run
sleep 10

account_id=$( curl -s http://169.254.169.254/latest/meta-data/iam/info | jq -r '.InstanceProfileArn' | cut -d: -f 5 )
instance_id=$( curl http://169.254.169.254/latest/meta-data/instance-id )
private_ip=$( curl http://169.254.169.254/latest/meta-data/local-ipv4 )
availability_zone=$( curl http://169.254.169.254/latest/meta-data/placement/availability-zone  )
region="${availability_zone%?}"
salt_branch=''
account_prefix=''

# Get the ASG name - can take a few tries
asg_group_name=''; tries=0; max_tries=5; step=10;
while test -z "$asg_group_name" && test "$tries" -lt "$max_tries"; do
    ((tries++))
    result="$( aws ec2 describe-tags --region "$region" \
        --filter "Name=resource-id,Values=$instance_id" \
        --query 'Tags[?Key==`aws:autoscaling:groupName`]' \
        | jq -r .[].Value )"
    test -n "$result" && { asg_group_name="$result"; break; }
    sleep "$step"
done
vpc=$( printf "$asg_group_name" | cut -d'-' -f 1 )
app=$( printf "${asg_group_name%Auto*}" | cut -d'-' -f 2 | tr '[:upper:]' '[:lower:]' )

# Figure out which Salt branch to use for bootstrapping
case $account_id in
    187977318227)
        if grep -qE "(production|openedx)" <<< "$vpc"; then 
            salt_branch='master'
        elif grep -q "operations" <<< "$vpc"; then 
            salt_branch='development'
        else 
            salt_branch="$vpc"
        fi
        account_prefix='ts'
    ;;
    697737431745)  
        if grep -q "testing" <<< "$vpc"; then 
            salt_branch='tsdev'
        elif grep -q "staging" <<< "$vpc"; then 
            salt_branch='tsdev'
        elif grep -q "qa" <<< "$vpc"; then
            salt_branch='tsdev'
        else 
            salt_branch='tsdev'
        fi
        account_prefix='tsdev'
    ;;
esac
host_id="${app}-${private_ip//./-}.${vpc}.${availability_zone}.${account_prefix}.local"

# Update instance 'Name' tag
aws ec2 create-tags --region "$region" --resources "$instance_id" --tags "Key=Name,Value=${host_id}"

# Print out updated host_id and minion configuration
printf "$host_id\n" > /opt/ts/id
printf "id: ${host_id}\nfile_client: local\nfile_roots:\n  base:\n    - /srv/salt\n" > /etc/salt/minion

# Update Salt code
export GIT_SSH=/opt/ts/git/git_ssh
pushd /opt/ts/git/ts-bootstrap > /dev/null 2>&1
    git clean -d -f
    git fetch --all
    git checkout "$salt_branch"
    git reset --hard "origin/$salt_branch"
    git pull
popd > /dev/null 2>&1

# Run Salt highstate
salt-call state.highstate

# Kernel update
sleep 10
while :; do
    if grep -q "[h]ighstate" < <(ps uax); then
        sleep 60
    else
        pushd /tmp &> /dev/null
            yum -y remove "kernel-*"
            rpm_file="kernel-lt-5.4.244-1.el7.elrepo.x86_64.rpm"
            curl "https://bitbucket.org/teachingstrategies/public-bootstrap/raw/master/rpms/$rpm_file" --output "$rpm_file" --silent  
            sleep 5; 
            rpm -U --force "$rpm_file"
            rm -f "$rpm_file"
        popd &> /dev/null
        grub2-mkconfig -o /boot/grub2/grub.cfg
        shutdown -r now
    fi
done

}
