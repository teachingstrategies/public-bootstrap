#!/bin/bash
# Note: this is a distilled version of the autoscaling script for the Jenkins bulid environment.

{ #Ensure the whole script is run

private_ip=$( curl http://169.254.169.254/latest/meta-data/local-ipv4 )
availability_zone=$( curl http://169.254.169.254/latest/meta-data/placement/availability-zone  )
salt_branch='qa'
host_id="jenkins-buildnodes-${private_ip//./-}.techops.${availability_zone}.tsdev.local"

# Print out updated host_id and minion configuration
printf -- '%s\n' "$host_id" > /opt/ts/id
printf -- 'id: %s\nfile_client: local\nfile_roots:\n  base:\n    - /srv/salt\n' "$host_id" > /etc/salt/minion

# Update Salt code
export GIT_SSH=/opt/ts/git/git_ssh
pushd /opt/ts/git/ts-bootstrap > /dev/null 2>&1
    git clean -d -f
    git fetch --all
    git checkout "$salt_branch"
    git reset --hard "origin/$salt_branch"
    git pull
popd > /dev/null 2>&1

# Run Salt highstate
salt-call state.highstate

}

